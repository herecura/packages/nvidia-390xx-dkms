# Maintainer: Jonathon Fernyhough <jonathon+m2x+dev>
# Contributor: Alonso Rodriguez <alonsorodi20 (at) gmail (dot) com>
# Contributor: James Rayner <iphitus@gmail.com>
# Contributor: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>

pkgname=nvidia-390xx-dkms
pkgver=390.157
pkgrel=11
pkgdesc="NVIDIA dkms drivers for linux, 390xx legacy branch"
arch=('x86_64')
url="https://www.nvidia.com/"
conflicts=('nvidia')
license=('custom')
options=('!strip')
_pkg="NVIDIA-Linux-x86_64-${pkgver}-no-compat32"
source=("https://us.download.nvidia.com/XFree86/Linux-x86_64/${pkgver}/${_pkg}.run"
        'kernel-4.16.patch'
        'kernel-6.2.patch'
        'kernel-6.3.patch'
        'kernel-6.4.patch'
        'kernel-6.5.patch'
        'kernel-6.6.patch')
sha512sums=('f3526816807c726a6d5cc40f080cf511606f8afb27fb71b86302c9d8d529c2be3b177d2ecc770dd9ff400abc313d062eaa5233dec30d99ba313d7595353e67f0'
            'c04eb3b69dec3c3f7af45403f338559a48252146ce29516edf609b863593a27004514688c1ade3bd0eeaf87a15feec1445048cee50d2777bb715426e92e209c6'
            'c92b02dfce4c0b40cd10e196084d1392c8db7275be43fe11dfd08627e7a2239e573f7a1d248b213ad62f0f81c16b55d54b77b6ac45ee2c310b88542ae8c5010a'
            '41a043479e8f865be0058e93c74f8e2f3de7eb006264a9aafa98699ce6b168239c966cf77b75a120812db79629e061d38904e099a08481b739549113cfeb51bd'
            '133b24a1e58a75739510a7f5ae2ded0c06adac71cff7ff649dc4da7d14de98fbbf17456cc6e598d128f340a2a5fb797be79b732c1f5894f7b8358e1ea6798cb3'
            'c98fd069260003a74e2d9be82107998c8728b03e95863042d37932154d2736803ae3e94a4f3b29c734ce27a05cf610521911b6b941a1e5d0da1d38f3df5c8113'
            '8e1478e42b68a07bc1266cfbf827f17173ac6a519748ffbdd6083356a1a140a2afdd07c04fff90f66bf38f8593d616221b6a9d01b02d5703a755c8f2085f060a')

prepare() {
    sh "${_pkg}.run" --extract-only
    cd "${_pkg}"

    # Restore phys_to_dma support (still needed for 396.18)
    # https://bugs.archlinux.org/task/58074
    patch -Np1 -i "$srcdir/kernel-4.16.patch"

    cd kernel

    echo "kernel 6.2"
    patch -Np1 -i "$srcdir/kernel-6.2.patch"
    echo "kernel 6.3"
    patch -Np1 -i "$srcdir/kernel-6.3.patch"
    echo "kernel 6.4"
    patch -Np1 -i "$srcdir/kernel-6.4.patch"
    echo "kernel 6.5"
    patch -Np1 -i "$srcdir/kernel-6.5.patch"
    echo "kernel 6.6"
    patch -Np1 -i "$srcdir/kernel-6.6.patch"

    sed -i "s/__VERSION_STRING/${pkgver}/" dkms.conf
    sed -i 's/__JOBS/`nproc`/' dkms.conf
    sed -i 's/__DKMS_MODULES//' dkms.conf
    sed -i '$iBUILT_MODULE_NAME[0]="nvidia"\
DEST_MODULE_LOCATION[0]="/kernel/drivers/video"\
BUILT_MODULE_NAME[1]="nvidia-uvm"\
DEST_MODULE_LOCATION[1]="/kernel/drivers/video"\
BUILT_MODULE_NAME[2]="nvidia-modeset"\
DEST_MODULE_LOCATION[2]="/kernel/drivers/video"\
BUILT_MODULE_NAME[3]="nvidia-drm"\
DEST_MODULE_LOCATION[3]="/kernel/drivers/video"' dkms.conf

    # Gift for linux-rt guys
    sed -i 's/NV_EXCLUDE_BUILD_MODULES/IGNORE_PREEMPT_RT_PRESENCE=1 NV_EXCLUDE_BUILD_MODULES/' dkms.conf
}

package() {
    pkgdesc="NVIDIA driver sources for linux, 390xx legacy branch"
    depends=('dkms' "nvidia-390xx-utils=$pkgver" 'libglvnd')
    provides=('NVIDIA-MODULE')
    conflicts+=('nvidia-390xx')

    cd ${_pkg}

    install -dm 755 "${pkgdir}"/usr/src
    cp -dr --no-preserve='ownership' kernel \
        "${pkgdir}/usr/src/nvidia-${pkgver}"

    echo "blacklist nouveau" |
        install -Dm644 /dev/stdin "${pkgdir}/usr/lib/modprobe.d/${pkgname}.conf"

    install -Dt "${pkgdir}/usr/share/licenses/${pkgname}" \
        -m644 "${srcdir}/${_pkg}/LICENSE"
}
